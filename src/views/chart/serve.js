const lineptions = {
  name: 'lineRef',
  chartData: {
    columns: ['date', 'PV', 'order', 'OrderTotalRMB'],
    rows: [
      { date: '2010', PV: 1393, order: 3492, OrderTotalRMB: 3200 },
      { date: '2011', PV: 3530, order: 3230, OrderTotalRMB: 2600 },
      { date: '2012', PV: 2923, order: 2623, OrderTotalRMB: 5600 },
      { date: '2013', PV: 1723, order: 1423, OrderTotalRMB: 4900 },
      { date: '2014', PV: 3792, order: 1093, OrderTotalRMB: 3200 },
      { date: '2015', PV: 4593, order: 4293, OrderTotalRMB: 7800 }
    ]
  },
  setting: {
    metrics: ['PV', 'order', 'OrderTotalRMB'], // 指标 Y轴
    dimension: ['date'], // 维度 - X轴
    labelMap: {
      PV: '访问用户',
      order: '下单用户',
      OrderTotalRMB: '下单总金额'
    }
  }
}

const pieOptions = {
  name: 'pieRef',
  chartData: {
    columns: ['date', 'PV'],
    rows: [
      { date: '2010', PV: 1393 },
      { date: '2011', PV: 3530 },
      { date: '2012', PV: 2923 },
      { date: '2013', PV: 1723 },
      { date: '2014', PV: 3792 },
      { date: '2015', PV: 4593 }
    ]
  },
  setting: {
    // roseType: 'area', // 玫瑰图
    dimension: 'date', // 图例展示的值
    metrics: 'PV', // 饼图展示的数据
    // 参考 echarts
    label: {
      formatter: '{b}年-访问量：{c}'
    }
  }
}

const histogramOptions = {
  name: 'histogramRef',
  chartData: {
    columns: ['date', 'PV', 'order'],
    rows: [
      { date: '2010', PV: 1393, order: 1093, OrderTotalRMB: 3000 },
      { date: '2011', PV: 3530, order: 3230, OrderTotalRMB: 4000 },
      { date: '2012', PV: 2923, order: 2623, OrderTotalRMB: 3600 },
      { date: '2013', PV: 1723, order: 1423, OrderTotalRMB: 4500 },
      { date: '2014', PV: 3792, order: 3492, OrderTotalRMB: 5000 },
      { date: '2015', PV: 4593, order: 4293, OrderTotalRMB: 5500 }
    ]
  },
  grid: {
    show: true,
    top: 50,
    left: 10,
    backgroundColor: 'pink',
    borderColor: 'skyblue'
  },
  colors: ['#e66465', '#9198e5', '#e664e3'],
  extend: {
    series: {
      barWidth: 20 // 柱状图宽度
    }
  },
  setting: {
    metrics: ['PV', 'order', 'OrderTotalRMB'], // 指标 Y轴
    dimension: ['date'], // 维度 - X轴
    labelMap: {
      PV: '访问用户',
      order: '下单用户',
      OrderTotalRMB: '下单率'
    },
    // PV - 访问用户 - 自定义
    legendName: {
      访问用户: '访问用户Total: 10000'
    },
    axisSite: { right: ['order'] },
    yAxisType: ['KMB', 'KMB'],
    yAxisName: ['数值', '比率'],
    showLine: ['OrderTotalRMB'], // order 柱状图 转 折线图
    itemStyle: {},
    showBackground: true,
    stack: { 用户: ['PV', 'order'] },
    // 参考 echarts
    label: {
      formatter: '{b} {c}'
    }
  }
}

const barOptions = {
  name: 'barRef',
  chartData: {
    columns: ['date', 'PV', 'order'],
    rows: [
      { date: '2010', PV: 1393, order: 1093, OrderTotalRMB: 3200 },
      { date: '2011', PV: 3530, order: 4293, OrderTotalRMB: 2600 },
      { date: '2012', PV: 2923, order: 3492, OrderTotalRMB: 7600 },
      { date: '2013', PV: 1723, order: 1423, OrderTotalRMB: 4900 },
      { date: '2014', PV: 3792, order: 2623, OrderTotalRMB: 3200 },
      { date: '2015', PV: 4593, order: 3230, OrderTotalRMB: 7800 }
    ]
  },
  setting: {
    metrics: ['PV', 'order', 'OrderTotalRMB'], // 指标 Y轴
    dimension: ['date'], // 维度 - X轴
    // 排序
    dataOrder: {
      label: 'order',
      order: 'desc'
    },
    labelMap: {
      PV: '访问用户',
      order: '下单用户',
      OrderTotalRMB: '下单率'
    },
    xAxisType: ['KMB', 'KMB'],
    // PV - 访问用户 - 自定义
    legendName: {
      下单用户: '下单用户: 10000'
    },
    showLine: ['OrderTotalRMB'] // order 柱状图 转 折线图
  },
  extend: {
    'xAxis.0.axisLabel.rotate': 45
  }
}

const ringOptions = {
  name: 'ringRef',
  chartData: {
    columns: ['date', 'PV'],
    rows: [
      { date: '2010年', PV: 1393 },
      { date: '2011年', PV: 3530 },
      { date: '2012年', PV: 2923 },
      { date: '2013年', PV: 1723 },
      { date: '2014年', PV: 3792 },
      { date: '2015年', PV: 4593 }
    ]
  },
  setting: {
    roseType: 'area', // 玫瑰图
    // 参考 echarts
    label: {
      formatter: '访问量：{c}'
    },
    radius: [50, 100],
    offsetY: 200
  }
}

const waterfallOptions = {
  name: 'waterfallRef',
  chartData: {
    columns: ['active', 'time'],
    rows: [
      { active: '吃饭', time: 4 },
      { active: '睡觉', time: 10 },
      { active: '打豆豆', time: 5 }
    ]
  },
  setting: {
    dimension: 'active',
    metrics: 'active',
    labelMap: {
      time: '时间'
    },
    // 设置 总计、剩余 的名称
    totalNum: 24, // 指定总计值
    totalName: '总时间',
    remainName: '剩余时间'
  }
}

const funnelFallOptions = {
  name: 'funnelFallRef',
  chartData: {
    columns: ['status', 'value'],
    rows: [
      { status: '展示', value: 900 },
      { status: '访问', value: 600 },
      { status: '点击', value: 300 },
      { status: '订单', value: 100 }
    ]
  },
  setting: {
    dimension: 'status',
    metrics: 'value'
  }
}

const radarOptions = {
  name: 'radarRef',
  chartData: {
    columns: ['date', 'PV', 'order', 'orderRate'],
    rows: [
      { date: '2010', PV: 1393, order: 1093, orderRate: 0.32 },
      { date: '2011', PV: 3530, order: 3230, orderRate: 0.26 },
      { date: '2012', PV: 2923, order: 2623, orderRate: 0.76 },
      { date: '2013', PV: 1723, order: 1423, orderRate: 0.49 },
      { date: '2014', PV: 3792, order: 3492, orderRate: 0.32 },
      { date: '2015', PV: 4593, order: 4293, orderRate: 0.78 }
    ]
  },
  setting: {
    labelMap: {
      PV: '访问用户',
      order: '下单用户',
      orderRate: '下单率'
    }
  }
}

const scatterOptions = {
  name: 'scatterRef',
  chartData: {
    columns: ['date', 'PV', 'order', 'Age'],
    rows: [
      { date: '2010', PV: 1393, order: 1093, Age: 18 },
      { date: '2011', PV: 3530, order: 3230, Age: 16 },
      { date: '2012', PV: 2923, order: 2623, Age: 19 },
      { date: '2013', PV: 1723, order: 1423, Age: 20 },
      { date: '2014', PV: 3792, order: 3492, Age: 21 },
      { date: '2015', PV: 4593, order: 4293, Age: 22 }
    ]
  },
  setting: {}
}

export {
  lineptions,
  pieOptions,
  histogramOptions,
  barOptions,
  ringOptions,
  waterfallOptions,
  funnelFallOptions,
  radarOptions,
  scatterOptions
}
