import {
  Button,
  message,
  notification,
  Layout,
  ConfigProvider,
  Row,
  Col,
  Menu,
  Avatar,
  Breadcrumb,
  Icon,
  Dropdown,
  Select,
  Card,
  Form,
  FormModel,
  Input,
  InputNumber,
  Alert,
  Table,
  Modal,
  Divider,
  Popconfirm,
  Drawer,
  Spin,
  Tree,
  Result,
  Checkbox,
  TreeSelect,
  Tooltip,
  Descriptions,
  Badge
} from 'ant-design-vue'

export function useAnted(Vue) {
  Vue.prototype.$confirm = Modal.confirm
  Vue.prototype.$message = message
  Vue.prototype.$notification = notification
  Vue.prototype.$info = Modal.info
  Vue.prototype.$success = Modal.success
  Vue.prototype.$error = Modal.error
  Vue.prototype.$warning = Modal.warning

  Vue.use(Button)
    .use(Layout)
    .use(ConfigProvider)
    .use(Row)
    .use(Col)
    .use(Select)
    .use(Card)
    .use(Menu)
    .use(Avatar)
    .use(Breadcrumb)
    .use(Icon)
    .use(Dropdown)
    .use(Form)
    .use(FormModel)
    .use(Input)
    .use(Alert)
    .use(Table)
    .use(Modal)
    .use(Divider)
    .use(Popconfirm)
    .use(Drawer)
    .use(Spin)
    .use(Tree)
    .use(Result)
    .use(Checkbox)
    .use(TreeSelect)
    .use(InputNumber)
    .use(Tooltip)
    .use(Descriptions)
    .use(Badge)
}

process.env.NODE_ENV !== 'production' && console.warn('[ant-design]:  Antd use lazy-load. ')
