import Vue from 'vue'
import App from './App.vue'
import { createRouter } from './router'
import store from './store'

// 引入时间插件
import moment from 'moment'
// 引入请求封装
import HttpRequest from '@/utils/request'
// 引入ant Design组件库
import { useAnted } from '@/plugins/anted'

import 'nprogress/nprogress.css'
import 'v-charts/lib/style.css'

// 注册Vue 过滤器
import '@/utils/filter'
//注册mock
import './mock'

// 注册公共组件
import '@/components'

// vue注册
Vue.use(HttpRequest)
useAnted(Vue)

// vue原型挂载方法
Vue.prototype.$moment = moment

Vue.config.productionTip = false

new Vue({
  router: createRouter(),
  store,
  render: h => h(App)
}).$mount('#app')
