import { SIDEBAR_TYPE } from '../mutation-types'

const app = {
  state: {
    sideCollapsed: false,
    axiosCancelArr: []
  },
  mutations: {
    [SIDEBAR_TYPE]: (state, type) => {
      state.sideCollapsed = type
    },
    PUSH_CANCEL: (state, cancel) => {
      state.axiosCancelArr.push(cancel.cancelToken)
    },
    CLERR_CANCEL: state => {
      state.axiosCancelArr.forEach(e => {
        typeof e === 'function' && e()
      })
      state.axiosCancelArr = []
    }
  },
  actions: {
    pushCancel({ commit }, cancel) {
      commit('PUSH_CANCEL', cancel)
    },
    clearCancel({ commit }) {
      commit('CLERR_CANCEL')
    }
  }
}

export default app
