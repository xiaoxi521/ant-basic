import storage from 'store'
import { ACCESS_TOKEN, SERVE_MODE, TABS_ROUTES } from '@/store/mutation-types'
import { welcome } from '@/utils/util'
import { login, getUserInfo, getRolesInfo } from '@/api/user'

const user = {
  state: {
    token: storage.get(ACCESS_TOKEN),
    name: '',
    welcome: '',
    avatar: '',
    roles: [], // 菜单权限集合
    abilityRoles: [], // 按钮权限集合
    info: {},
    serveList: [],
    serve: storage.get(SERVE_MODE) || undefined
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_ABILITYROLES: (state, abilityRoles) => {
      state.abilityRoles = abilityRoles
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_SERVE: (state, serve) => {
      state.serve = serve
    },
    SET_SERVE_LIST: (state, serveList) => {
      state.serveList = serveList
    }
  },
  actions: {
    // 当前服务
    setServe({ commit }, serve) {
      commit('SET_SERVE', serve)
      storage.set(SERVE_MODE, serve, 7 * 24 * 60 * 60 * 1000)
    },
    // 服务列表
    setRoles({ commit }) {
      return new Promise((reslove, reject) => {
        getRolesInfo({
          serve: this.state.user.serve
        })
          .then(result => {
            commit('SET_ROLES', result.data.permissions || [])
            commit('SET_ABILITYROLES', result.data.abilityRoles || [])
            reslove(result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then(response => {
            storage.set(ACCESS_TOKEN, response.data.token, 7 * 24 * 60 * 60 * 1000)
            commit('SET_TOKEN', response.data.token)
            resolve()
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    // 获取用户信息
    GetInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then(response => {
            const result = response.data
            if (result) {
              commit('SET_INFO', result)
              // 设置服务列表
              commit('SET_SERVE_LIST', result.serveList)
              // 设置第一条为默认服务
              if (result.serveList && result.serveList[0] && !storage.get(SERVE_MODE)) {
                commit('SET_SERVE', result.serveList[0].value)
              }
              commit('SET_NAME', { name: result.nickName, welcome: welcome() })
              commit('SET_AVATAR', result.headImg)
              resolve(response)
            } else {
              reject(new Error('getInfo: roles must be a non-null array !'))
            }
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    // 登出
    Logout({ commit }) {
      return new Promise(resolve => {
        commit('SET_INFO', {})
        commit('SET_NAME', '')
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_ABILITYROLES', [])
        commit('SET_SERVE_LIST', [])
        commit('SET_SERVE', undefined)
        storage.remove(ACCESS_TOKEN)
        storage.remove(SERVE_MODE)
        storage.remove(TABS_ROUTES)
        resolve()
      })
    }
  }
}

export default user
