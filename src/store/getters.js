const getters = {
  token: state => state.user.token,
  name: state => state.user.name,
  avatar: state => state.user.avatar,
  welcome: state => state.user.welcome,
  roles: state => state.user.roles,
  abilityRoles: state => state.user.abilityRoles,
  userInfo: state => state.user.info,
  serve: state => state.user.serve,
  serveList: state => state.user.serveList,
  menus: state => state.permission.menus,
  keepAliveComponents: state => state.permission.keepAliveComponents,
  tabsList: state => state.tabsView.tabsList
}

export default getters
