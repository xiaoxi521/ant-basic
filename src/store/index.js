import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)

// 读取modules的目录,不读子目录,匹配.js文件的正则表达式
const files = require.context('./modules', false, /\.js$/)
const modules = {}
files.keys().forEach(key => {
  const store = files(key).default
  const moduleName = key.replace(/\.\//, '').replace(/\.js/, '')
  modules[moduleName] = store
  modules[moduleName].namespaced = true
})

export default new Vuex.Store({
  modules,
  state: {},
  mutations: {},
  actions: {},
  getters
})
