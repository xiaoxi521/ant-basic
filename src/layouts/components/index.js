import NavMenu from './NavMenu.jsx'
import GloablHeader from './GlobalHeader/index.vue'
import GlobalFooter from './GlobalFooter/index.vue'
import GlobalLogo from './GlobalLogo/index.vue'
import { TabsView } from './tabs/index'

export { NavMenu, GloablHeader, GlobalFooter, GlobalLogo, TabsView }
