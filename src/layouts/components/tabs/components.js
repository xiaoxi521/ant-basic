import { Dropdown, Tabs, Card } from 'ant-design-vue'

export default {
  [Tabs.name]: Tabs,
  [Tabs.TabPane.name]: Tabs.TabPane,
  [Dropdown.name]: Dropdown,
  [Card.name]: Card
}
