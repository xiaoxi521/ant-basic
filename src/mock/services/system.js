import Mock from 'mockjs2'
import { builder, getBody } from '../util'

const menuInfo = () => {
  const menu = [
    {
      menuId: 117,
      name: '仪表盘',
      roleId: 'dashboard',
      permission: null,
      parentId: null,
      sortNum: 10,
      createTime: null,
      updateTime: 1608723513000,
      children: [
        {
          menuId: 170,
          name: '欢迎页',
          roleId: 'dashboard/welcome',
          permission: null,
          parentId: 117,
          sortNum: 1,
          createTime: 1610336552459,
          updateTime: 1610336552000,
          children: null
        },
        {
          menuId: 161,
          name: '工作台',
          roleId: 'dashboard/workbench',
          permission: null,
          parentId: 117,
          sortNum: 0,
          createTime: 1608695358056,
          updateTime: 1608695358000,
          children: null
        }
      ]
    },
    {
      menuId: 142,
      name: '话题管理',
      roleId: 'question',
      permission: null,
      parentId: null,
      sortNum: 9,
      createTime: null,
      updateTime: 1608723533000,
      children: [
        {
          menuId: 143,
          name: '创业问答',
          roleId: 'question/index',
          permission: null,
          parentId: 142,
          sortNum: 0,
          createTime: null,
          updateTime: 1608743317000,
          children: null
        },
        {
          menuId: 144,
          name: '分享管理',
          roleId: 'share/index',
          permission: null,
          parentId: 142,
          sortNum: 0,
          createTime: 1608534782685,
          updateTime: 1608563644000,
          children: null
        },
        {
          menuId: 168,
          name: '问答管理',
          roleId: 'answer/list',
          permission: null,
          parentId: 142,
          sortNum: 0,
          createTime: 1608999867017,
          updateTime: 1608999867000,
          children: null
        }
      ]
    },
    {
      menuId: 151,
      name: '用户管理',
      roleId: 'avatar',
      permission: null,
      parentId: null,
      sortNum: 5,
      createTime: null,
      updateTime: 1608723568000,
      children: [
        {
          menuId: 152,
          name: '用户列表',
          roleId: 'avatar/list',
          permission: null,
          parentId: 151,
          sortNum: 0,
          createTime: 1608535099651,
          updateTime: 1608535100000,
          children: null
        },
        {
          menuId: 153,
          name: '角色管理',
          roleId: 'system/role',
          permission: null,
          parentId: 151,
          sortNum: 0,
          createTime: 1608535112687,
          updateTime: 1608535113000,
          children: null
        },
        {
          menuId: 154,
          name: '权限配置',
          roleId: 'system/juris',
          permission: null,
          parentId: 151,
          sortNum: 0,
          createTime: 1608535144254,
          updateTime: 1608564411000,
          children: null
        }
      ]
    }
  ]

  return builder(menu)
}

const roleLost = () => {
  const list = [
    {
      id: 1,
      code: 'a0001',
      name: '超级管理员',
      createTime: '2020-10-11'
    },
    {
      id: 2,
      code: 'a0002',
      name: '测试用户',
      createTime: '2020-10-11'
    },
    {
      id: 3,
      code: 'a0003',
      name: '用户',
      createTime: '2020-10-11'
    }
  ]
  return builder(list)
}

Mock.mock(/\/system\/menu/, 'post', menuInfo)
Mock.mock(/\/system\/role/, 'post', roleLost)
