import Mock from 'mockjs2'
import { builder } from '../util'

const totalSales = () => {
  const data = Mock.mock({
    columns: ['date', 'PV', 'Order', 'OrderRate'],
    'rows|4': [
      {
        date: '@date',
        'PV|1-8000': 8000,
        'Order|1-8000': 8000,
        'OrderTotalRMB|1-80000': 80000
      }
    ]
  })
  return builder(data, 'success', 0)
}

Mock.mock(/\/total\/sales/, 'post', totalSales)
