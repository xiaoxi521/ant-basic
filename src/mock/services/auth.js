import Mock from 'mockjs2'
import { builder, getBody } from '../util'

const logout = () => {
  return builder({}, '[测试接口] 注销成功')
}

const login = _params => {
  const params = getBody(_params)
  console.log(params)
  if (params.username === 'admin' && params.password === 'admin') {
    return builder({ token: 'Bjcxknvlkdnjn.asdhfkhk.jxchvksjdfjsdkf==' }, '[测试接口] 登录成功', 0)
  } else {
    return builder(null, '[测试接口] 登录失败', -1)
  }
}

Mock.mock(/\/auth\/login/, 'post', login)
Mock.mock(/\/auth\/logout/, 'post', logout)
