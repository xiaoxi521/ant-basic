import Mock from 'mockjs2'
import { builder, getBody } from '../util'

const articleList = _params => {
  const params = getBody(_params)

  const data = Mock.mock({
    'list|10': [
      {
        'id|+1': 1,
        cover: 'https://gw.alipayobjects.com/zos/rmsportal/WdGqmHpayyMjiEhcKoVE.png',
        range: 1,
        title: '那是一种内在的东西， 他们到达不了，也无法触及的',
        createTime: `@datetime`
      }
    ],
    pageSize: 10,
    pageNum: params.pageNum || 1,
    totalPage: 3,
    total: 30
  })

  return builder(data, 'success', 0)
}

Mock.mock(/\/article\/list/, 'post', articleList)
