import Mock from 'mockjs2'
import { builder, getBody } from '../util'

const info = () => {
  const userInfo = {
    id: '4291d7da9005377ec9aec4a71ea837f',
    name: '天野远子',
    username: 'admin',
    password: '',
    avatar: 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
    status: 1,
    telephone: '',
    lastLoginIp: '27.154.74.117',
    lastLoginTime: 1534837621348,
    creatorId: 'admin',
    createTime: 1497160610259,
    merchantCode: 'TLif2btpzg079h15bk',
    deleted: 0,
    roleId: 'admin',
    role: {},
    IP: '127.0.0.1'
  }
  // serve
  const serveList = [
    { name: 'serve one', value: 1 },
    { name: 'serve two', value: 2 },
    { name: 'serve three', value: 3 }
  ]

  userInfo.serveList = serveList
  return builder(userInfo)
}

const roles = _params => {
  const params = getBody(_params)
  // role
  const roles = {
    id: 'admin',
    name: '管理员',
    describe: '拥有所有权限',
    status: 1,
    creatorId: 'system',
    createTime: 1497160610259,
    deleted: 0,
    permissions: []
  }

  const permissions = [
    {
      roleId: 'admin',
      permissionId: 'dashboard',
      permissionName: '仪表盘'
    },
    {
      roleId: 'admin',
      permissionId: 'exception',
      permissionName: '警告'
    }
  ]

  const permissions2 = [
    {
      roleId: 'admin',
      permissionId: 'dashboard',
      permissionName: '仪表盘'
    }
  ]

  const permissions3 = [
    {
      roleId: 'admin',
      permissionId: 'chart',
      permissionName: '警告'
    }
  ]

  switch (params.serve) {
    case 1:
      roles.permissions = permissions
      break
    case 2:
      roles.permissions = permissions2
      break
    case 3:
      roles.permissions = permissions3
      break
  }

  return builder(roles)
}

Mock.mock(/\/user\/get/, 'post', info)
Mock.mock(/\/roles\/get/, 'post', roles)
