import Http from '@/utils/request'

const apiConfig = {
  articleList: '/article/list'
}

/**
 * @param {Object} _params
 */
export function getArticleList(_params = {}) {
  return Http.post(apiConfig.articleList, {
    data: _params
    // loadingEl: true // 开启全屏loading
  })
}
