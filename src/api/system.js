import Http from '@/utils/request'

const apiConfig = {
  systemMenu: '/system/menu', // 菜单设置
  systemRole: '/system/role'
}

// 获取菜单
export function getMenuList(_params = {}) {
  return Http.post(apiConfig.systemMenu, {
    data: _params
    // loadingEl: true, // 开启全屏loading
  })
}

// 获取角色列表
export function getRoleList(_params = {}) {
  return Http.post(apiConfig.systemRole, {
    data: _params
    // loadingEl: true, // 开启全屏loading
  })
}
