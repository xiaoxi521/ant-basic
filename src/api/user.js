import Http from '@/utils/request'

const apiConfig = {
  Login: '/auth/login', // 登录
  Logout: '/auth/logout', // 登出
  userInfo: '/user/get', // 获取用户信息
  rolesInfo: '/roles/get' // 获取权限
}

/**
 * @param {Object} _params
 */
export function login(_params = {}) {
  return Http.post(apiConfig.Login, {
    data: _params
    // loadingEl: true, // 开启全屏loading
  })
}

// 登出
export function logout(_params = {}) {
  return Http.post(apiConfig.Logout, {
    data: _params
  })
}

// 获取用户信息
export function getUserInfo(_params = {}) {
  return Http.post(apiConfig.userInfo, {
    data: _params
    // loadingEl: true, // 开启全屏loading
  })
}

// 获取用户权限
export function getRolesInfo(_params = {}) {
  return Http.post(apiConfig.rolesInfo, {
    data: _params
    // loadingEl: true, // 开启全屏loading
  })
}
