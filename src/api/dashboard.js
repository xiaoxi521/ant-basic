import Http from '@/utils/request'

const apiConfig = {
  totalSales: '/total/sales'
}

/**
 * @param {Object} _params
 */
export function getTotalSales(_params = {}) {
  return Http.post(apiConfig.totalSales, {
    data: _params
    // loadingEl: true // 开启全屏loading
  })
}
