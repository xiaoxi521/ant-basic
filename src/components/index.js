import Vue from 'vue'
import STable from './Table'
import GlobalBreadCrumb from './GlobalBreadCrumb'
import PageLoading from './PageLoading/index'
import WangEditor from './WangEditor/index'

// 注册全局需要的组件
Vue.component('STable', STable)
Vue.component('PageLoading', PageLoading)

// 导出非全局使用组件
export { WangEditor, GlobalBreadCrumb }
