/**
 * v-chart参考文档 https://zizorg.github.io/v-charts/
 */
import vLine from 'v-charts/lib/line.common'
import vBar from 'v-charts/lib/bar.common'
import vFunnel from 'v-charts/lib/funnel.common'
import vPie from 'v-charts/lib/pie.common'
import vHistogram from 'v-charts/lib/histogram.common'
import vRadar from 'v-charts/lib/radar.common'
import vRing from 'v-charts/lib/ring.common'
import vWaterfall from 'v-charts/lib/waterfall.common'
import vMap from 'v-charts/lib/map.common'
import vSankey from 'v-charts/lib/sankey.common'
import vHeatmap from 'v-charts/lib/heatmap.common'
import vScatter from 'v-charts/lib/scatter.common'
import vCandle from 'v-charts/lib/candle.common'
import vGauge from 'v-charts/lib/gauge.common'
import vTree from 'v-charts/lib/tree.common'
import vBmap from 'v-charts/lib/bmap.common'
import vAmap from 'v-charts/lib/amap.common'

export {
  vLine,
  vBar,
  vFunnel,
  vPie,
  vHistogram,
  vRadar,
  vRing,
  vWaterfall,
  vMap,
  vSankey,
  vHeatmap,
  vScatter,
  vCandle,
  vGauge,
  vTree,
  vBmap,
  vAmap
}
