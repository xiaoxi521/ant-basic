import { Spin } from 'ant-design-vue'
import Vue from 'vue'
const Mask = Vue.extend(Spin)

// 在考虑是否需要单例模式，看具体使用情况在修改
// 返回loadingService服务状态的实例  Loading.service()方式调用
export const Loading = (() => {
  const Service = function() {
    let div
    let loadingMask
    this.service = function(options = {}) {
      const node = document.getElementsByClassName('ldx-axios-loading')
      if (node.length > 0) return this
      const { title, background } = options
      div = document.createElement('div')
      div.setAttribute('class', 'ldx-axios-loading')
      div.style.position = 'fixed'
      div.style.width = '100vw'
      div.style.height = '100vh'
      div.style.left = 0
      div.style.right = 0
      div.style.top = 0
      div.style.bottom = 0
      div.style.display = 'flex'
      div.style.alignItems = 'center'
      div.style.justifyContent = 'center'
      div.style.background = 'rgba(238, 238, 238,0.6)'
      div.style.zIndex = '999999'
      if (background) div.style.background = background
      document.body.appendChild(div)
      const child = document.createElement('div')
      div.appendChild(child)
      loadingMask = new Mask()
      loadingMask.spinning = true
      if (title) loadingMask.tip = title
      loadingMask.$mount(child)
      return this
    }
    this.close = function() {
      const node = document.getElementsByClassName('ldx-axios-loading')
      if (node.length > 0) {
        loadingMask.spinning = false
        div.remove()
      }
    }
  }
  return new Service()
})()
