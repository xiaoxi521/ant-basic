import Dashboard from './modules/dashboard'
import Redirect from './modules/redirect'
import Chart from './modules/chart'
import Form from './modules/form'
import System from './modules/system'
import Exception from './modules/exception'
import Link from './modules/link'

export default [...Dashboard, ...Chart, ...Form, ...System, ...Exception, ...Link, ...Redirect]
