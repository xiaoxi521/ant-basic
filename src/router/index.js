import Vue from 'vue'
import Router from 'vue-router'
import { BasicLayout } from '@/layouts'

import constantRouter from './modules/constantRouter'
import common from '@/router/common'

import { createRouterGuards } from './router-guards'

// 生成路由表
const routes = [
  {
    path: '/',
    name: 'Layout',
    redirect: '/dashboard',
    component: BasicLayout,
    meta: { title: '首页' },
    children: [...common]
  },
  ...constantRouter
]

// hack router push callback
const originalPush = Router.prototype.push
Router.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

const createRouter = () => {
  const router = new Router({
    mode: 'hash',
    routes: [...routes]
  })
  createRouterGuards(router)
  return router
}

export { routes, createRouter }
