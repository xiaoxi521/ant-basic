import RouteTransition from '@/layouts/RouteTransition'

const routeName = 'form'

const routes = [
  {
    path: '/form',
    name: routeName,
    redirect: '/form/index',
    component: RouteTransition,
    meta: {
      title: '表单',
      icon: 'table',
      permission: ['dashboard'],
      target: 'menuItem' // 一级菜单（用来优化组件缓存个动画）
    },
    children: [
      {
        path: '/form/index',
        name: `${routeName}-index`,
        meta: {
          title: '表单-Demo',
          permission: ['dashboard'],
          keepAlive: true,
          target: 'sonMenu' // 一级菜单的重定向子页面
        },
        component: () => import('@/views/form/index')
      }
    ]
  }
]

export default routes
