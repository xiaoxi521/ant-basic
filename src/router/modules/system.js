import RouteTransition from '@/layouts/RouteTransition'

const routeName = 'system'

const routes = [
  {
    path: '/system',
    name: routeName,
    redirect: '/system/avatarManage',
    meta: {
      title: '系统管理',
      icon: 'tool',
      permission: ['dashboard']
    },
    component: RouteTransition,
    children: [
      {
        path: '/system/avatarManage',
        name: `${routeName}-avatarManage`,
        meta: {
          title: '用户管理',
          permission: ['dashboard'],
          keepAlive: true
        },
        component: () => import('@/views/system/avatarManage/index')
      },
      {
        path: '/system/menuManage',
        name: `${routeName}-systemMenuManage`,
        meta: {
          title: '菜单管理',
          permission: ['dashboard'],
          keepAlive: true
        },
        component: () => import('@/views/system/menuManage/index')
      },
      {
        path: '/system/rolesManage',
        name: `${routeName}-systemRolesManage`,
        meta: {
          title: '角色管理',
          permission: ['dashboard'],
          keepAlive: true
        },
        component: () => import('@/views/system/rolesManage/index')
      }
    ]
  }
]

export default routes
