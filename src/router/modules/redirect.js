import RouteTransition from '@/layouts/RouteTransition'

const routes = [
  {
    path: '/redirect/:path*',
    name: 'Redirect',
    hidden: true,
    meta: {
      title: '刷新中',
      icon: 'SettingOutlined'
    },
    component: RouteTransition,
    children: [
      {
        path: '',
        name: 'Redirect',
        hidden: true,
        meta: {
          title: 'loading',
          keepAlive: false
        },
        component: () => import('@/views/redirect/index.vue')
      }
    ]
  }
]

export default routes
