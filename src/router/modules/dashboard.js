import RouteTransition from '@/layouts/RouteTransition'

const routeName = 'dashboard'

const routes = [
  {
    path: '/dashboard',
    name: routeName,
    redirect: '/dashboard/welcome',
    component: RouteTransition,
    meta: {
      title: 'dashboard',
      icon: 'dashboard',
      permission: ['dashboard']
    },
    children: [
      {
        path: '/dashboard/welcome',
        name: `${routeName}-welcome`,
        meta: {
          title: '首页',
          permission: ['dashboard'],
          keepAlive: false
        },
        component: () => import('@/views/dashboard/welcome/index')
      },
      {
        path: '/dashboard/workbench',
        name: `${routeName}-workbench`,
        meta: {
          title: '工作台',
          permission: ['dashboard'],
          keepAlive: true
        },
        component: () => import('@/views/dashboard/workbench/index')
      }
    ]
  }
]

export default routes
