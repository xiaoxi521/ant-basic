import RouteTransition from '@/layouts/RouteTransition'

const routeName = 'chart'

const routes = [
  {
    path: '/chart',
    name: routeName,
    redirect: '/chart/demo',
    component: RouteTransition,
    meta: {
      icon: 'picture',
      title: '图表',
      permission: ['dashboard']
    },
    children: [
      {
        path: '/chart/demo',
        name: `${routeName}-demo`,
        meta: {
          title: '图表-示例',
          permission: ['dashboard'],
          keepAlive: true
        },
        component: () => import('@/views/chart/index')
      },
      {
        path: '/chart/test',
        name: `${routeName}-test`,
        redirect: '/chart/test/first',
        meta: {
          title: '菜单测试',
          permission: ['dashboard']
        },
        component: RouteTransition,
        children: [
          {
            path: '/chart/test/first',
            name: `${routeName}-test-first`,
            meta: {
              title: '菜单测试 - 1',
              permission: ['dashboard'],
              keepAlive: true
            },
            component: () => import('@/views/chart/demo/index')
          }
        ]
      }
    ]
  }
]

export default routes
