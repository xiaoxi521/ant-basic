import RouteTransition from '@/layouts/RouteTransition'

const routeName = 'exception'

const routes = [
  {
    path: '/exception',
    name: routeName,
    redirect: '/exception/403',
    meta: { title: '异常页', icon: 'warning', permission: ['exception'] },
    component: RouteTransition,
    children: [
      {
        path: '/exception/403',
        name: `${routeName}-Exception403`,
        meta: { title: '403', permission: ['exception'] },
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403')
      },
      {
        path: '/exception/404',
        name: `${routeName}-Exception404`,
        meta: { title: '404', permission: ['exception'] },
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
      },
      {
        path: '/exception/500',
        name: `${routeName}-Exception500`,
        meta: { title: '500', permission: ['exception'] },
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500')
      }
    ]
  }
]

export default routes
