const webpack = require('webpack')
const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

console.log(`%c===============================`)
console.log(`%c|| 当前运行环境: ${process.env.NODE_ENV} ||`)
console.log(`%c|| 当前接口域名: ${process.env.VUE_APP_BASE_API} ||`)
console.log(`%c|| 打包输出文件名: ${process.env.VUE_APP_OUTDIR} ||`)
console.log(`%c===============================`)

module.exports = {
  // publicPath: './',
  outputDir: process.env.VUE_APP_OUTDIR,
  productionSourceMap: false,
  configureWebpack: {
    plugins: [
      // 优化moment打包文件，如果引入了./locale，忽略掉不引入，手动引入需要的语言包
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    ]
  },
  chainWebpack: config => {
    config.resolve.alias.set('@$', resolve('src'))
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('vue-svg-icon-loader')
      .loader('vue-svg-icon-loader')
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]'
      })
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // less vars，customize ant design theme
          'primary-color': '#1890ff', // 全局主色
          'link-color': '#1890ff', // 链接色
          'border-radius-base': '2px',
          'success-color': '#52c41a', // 成功色
          'warning-color': '#faad14', // 警告色
          'error-color': '#f5222d', // 错误色
          'font-size-base': '14px' // 主字号
        },
        javascriptEnabled: true
      }
    }
  }
  // 代理配置
  // devServer: {
  //   port: 8000,
  //   proxy: {
  //     '/api': {
  //       target: process.env.VUE_APP_BASE_API,
  //       ws: false,
  //       // pathRewrite表示路径重写，key表示一个正则，value表示别名
  //       // pathRewrite: {
  //       //   '^/api': '' // 即用 '/api'表示'http://localhost:3000/api'
  //       // },
  //       changeOrigin: true
  //     },
  //     '/oss': {
  //       target: process.env.VUE_APP_OSS_API,
  //       ws: false,
  //       // pathRewrite表示路径重写，key表示一个正则，value表示别名
  //       pathRewrite: {
  //         '^/oss': '' // 即用 '/api'表示'http://localhost:3000/api'
  //       },
  //       changeOrigin: true
  //     }
  //   }
  // }
}
